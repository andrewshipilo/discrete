#include <random>
#include <iostream>
#include "qcustomplot.h"

void plotHist(QCustomPlot* customPlot, const QVector<double> &x, const QVector<double> &y_th, const QVector<double> &y_ex)
{
    customPlot->resize(768, 768);

    // set dark background gradient:
    QLinearGradient plotGradient;
    plotGradient.setStart(0, 0);
    plotGradient.setFinalStop(0, 350);
    plotGradient.setColorAt(0, QColor(80, 80, 80));
    plotGradient.setColorAt(1, QColor(50, 50, 50));
    customPlot->setBackground(plotGradient);

    auto bars1 = new QCPBars(customPlot->xAxis, customPlot->yAxis);
    bars1->setName("Theoretical");
    bars1->setAntialiased(false);
    bars1->setWidth(18 / (double) y_th.size());
    bars1->setData(x, y_th);
    bars1->setPen(Qt::NoPen);
    bars1->setBrush(QColor(216, 169, 3, 255));

    auto bars2 = new QCPBars(customPlot->xAxis, customPlot->yAxis);
    bars2->setName("Experimental");
    bars2->setAntialiased(false);
    bars2->setWidth(9 / (double) y_ex.size());
    bars2->setData(x, y_ex);
    bars2->setPen(Qt::NoPen);
    bars2->setBrush(QColor(128, 0, 128, 255));

    customPlot->xAxis->setBasePen(QPen(Qt::white, 1));
    customPlot->yAxis->setBasePen(QPen(Qt::white, 1));
    customPlot->xAxis->setTickPen(QPen(Qt::white, 1));
    customPlot->yAxis->setTickPen(QPen(Qt::white, 1));
    customPlot->xAxis->setSubTickPen(QPen(Qt::white, 1));
    customPlot->yAxis->setSubTickPen(QPen(Qt::white, 1));
    customPlot->xAxis->setTickLabelColor(Qt::white);
    customPlot->yAxis->setTickLabelColor(Qt::white);
    customPlot->xAxis->grid()->setPen(QPen(QColor(140, 140, 140), 1, Qt::DotLine));
    customPlot->yAxis->grid()->setPen(QPen(QColor(140, 140, 140), 1, Qt::DotLine));
    customPlot->xAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    customPlot->yAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    customPlot->xAxis->grid()->setSubGridVisible(true);
    customPlot->yAxis->grid()->setSubGridVisible(true);
    customPlot->xAxis->grid()->setZeroLinePen(Qt::NoPen);
    customPlot->yAxis->grid()->setZeroLinePen(Qt::NoPen);
    customPlot->xAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);
    customPlot->yAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);
    customPlot->legend->setVisible(true);

    customPlot->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignRight | Qt::AlignTop);
    customPlot->legend->setBrush(QColor(255, 255, 255, 100));
    customPlot->legend->setBorderPen(Qt::NoPen);
    customPlot->legend->setFont(QFont("Helvetica", 9));

    customPlot->rescaleAxes();
    customPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);
    customPlot->show();

}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    int n = 500;

    QVector<double> x = {-88, -66.5, -26.2, -4.5, -0.3, 65.0, 65.8};
    std::vector<std::pair<double, int>> m = {{-88,   0},
                                             {-66.5, 0},
                                             {-26.2, 0},
                                             {-4.5,  0},
                                             {-0.3,  0},
                                             {65.0,  0},
                                             {65.8,  0}};
    QVector<double> p = {0.186, 0.246, 0.139, 0.157, 0.207, 0.015, 0.050};
    QVector<double> generated;

    std::mt19937 rng;
    rng.seed(std::random_device()());
    std::uniform_real_distribution<float> dist(0.f, 1.f);

    double mat = 0;
    double dis = 0;
    for (int i = 0; i < n; i++)
    {
        double rand = dist(rng);
        double cumulative = 0;
        for (int j = 0; j < p.size(); j++)
        {
            cumulative += p[j];
            if (rand < cumulative)
            {
                m[j].second++;
                generated.push_back(x[j]);
                mat += x[j];
                break;
            }
        }
    }
    mat /= n;

    double thMat = 0;
    double thDis = 0;
    for (int i = 0; i < p.size(); i++)
    {
        thMat += p[i] * x[i];
        thDis += p[i] * x[i] * x[i];
    }

    thDis -= thMat * thMat;

    for (int i = 0; i < n; i++)
    {
        dis += (generated[i] - mat) * (generated[i] - mat);
    }
    dis /= (n - 1);

    std::cout << "Mat: theor: " << thMat << " exper: " << mat << std::endl;
    std::cout << "Dis: theor: " << thDis << " exper: " << dis << std::endl;

    QVector<double> y_th(7), y_ex(7);

    for (int i = 0; i < x.size(); ++i)
    {
        y_th[i] = p[i];
        y_ex[i] = m[i].second / (double) n;
    }

    auto customPlot = new QCustomPlot();
    plotHist(customPlot, x, y_th, y_ex);

    a.exec();

    delete customPlot;

    return 0;
}